import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch import autograd
import time as t
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import os
from torchvision import utils
import numpy as np
from PIL import Image
import imageio
from utils.file_types import SFI, ABS, WRL


class Generator(torch.nn.Module):
    def __init__(self, channels, im_size = 64):
        super().__init__()
        # Filters [1024, 512, 256]
        # Input_dim = 100
        # Output_dim = C (number of channels)
        if im_size == 64:
            self.encoder = nn.Sequential(
                nn.Conv2d(in_channels=channels, out_channels=128, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(128, affine=True),
                nn.LeakyReLU(0.2, inplace=True),                
                
                # Image (128x32x32)
                nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(256, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (256x16x16)
                nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(512, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (512x8x8)
                nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(1024, affine=True),
                nn.LeakyReLU(0.2, inplace=True)
                )


            self.decoder = nn.Sequential(
    
                # State (1024x4x4)
                nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=512),
                nn.ReLU(True),
    
                # State (512x8x8)
                nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=256),
                nn.ReLU(True),
    
                # State (256x16x16)
                nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=128),
                nn.ReLU(True),
                
                # State (128x32x32)
                nn.ConvTranspose2d(in_channels=128, out_channels=channels, kernel_size=4, stride=2, padding=1)
                # output of main module --> Image (Cx64x64)
                )
            
        elif im_size == 128:
            self.encoder = nn.Sequential(
                nn.Conv2d(in_channels=channels, out_channels=128, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(128, affine=True),
                nn.LeakyReLU(0.2, inplace=True),                
                
                # Image (128x64x64)
                nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(256, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (256x32x32)
                nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(512, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (512x16x16)
                nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(1024, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
                
                
                # State (1024x8x8)
                nn.Conv2d(in_channels=1024, out_channels=2048, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(2048, affine=True),
                nn.LeakyReLU(0.2, inplace=True)
                )


            self.decoder = nn.Sequential(
                # State (1024x4x4)
                nn.ConvTranspose2d(in_channels=2048, out_channels=1024, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=1024),
                nn.ReLU(True),
                
                # State (1024x4x4)
                nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=512),
                nn.ReLU(True),
    
                # State (512x8x8)
                nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=256),
                nn.ReLU(True),
    
                # State (256x16x16)
                nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=4, stride=2, padding=1),
                nn.BatchNorm2d(num_features=128),
                nn.ReLU(True),
                
                # State (128x32x32)
                nn.ConvTranspose2d(in_channels=128, out_channels=channels, kernel_size=4, stride=2, padding=1)
                # output of main module --> Image (Cx64x64)
                )
        else:
            raise TypeError("unsupported imsize")

        self.output = nn.Tanh()


    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return self.output(x)

class Discriminator(torch.nn.Module):
    def __init__(self, channels, im_size = 64):
        super().__init__()
        # Filters [256, 512, 1024]
        # Input_dim = channels (Cx64x64)
        # Output_dim = 1
        if im_size == 64:
            self.main_module = nn.Sequential(
                # Omitting batch normalization in critic because our new penalized training objective (WGAN with gradient penalty) is no longer valid
                # in this setting, since we penalize the norm of the critic's gradient with respect to each input independently and not the enitre batch.
                # There is not good & fast implementation of layer normalization --> using per instance normalization nn.InstanceNorm2d()
    
                # Image (2Cx64x64)
                nn.Conv2d(in_channels=2*channels, out_channels=128, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(128, affine=True),
                nn.LeakyReLU(0.2, inplace=True),                
                
                # Image (128x32x32)
                nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(256, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (256x16x16)
                nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(512, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (512x8x8)
                nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(1024, affine=True),
                nn.LeakyReLU(0.2, inplace=True))
                # output of main module --> State (1024x4x4)
                    
                
            self.output = nn.Sequential(
                # The output of D is no longer a probability, we do not apply sigmoid at the output of D.
                nn.Conv2d(in_channels=1024, out_channels=1, kernel_size=4, stride=1, padding=0))
        
        elif im_size == 128:
            self.main_module = nn.Sequential(
                # Omitting batch normalization in critic because our new penalized training objective (WGAN with gradient penalty) is no longer valid
                # in this setting, since we penalize the norm of the critic's gradient with respect to each input independently and not the enitre batch.
                # There is not good & fast implementation of layer normalization --> using per instance normalization nn.InstanceNorm2d()
    
                # Image (2Cx64x64)
                nn.Conv2d(in_channels=2*channels, out_channels=128, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(128, affine=True),
                nn.LeakyReLU(0.2, inplace=True),                
                
                # Image (128x32x32)
                nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(256, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (256x16x16)
                nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(512, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
    
                # State (512x8x8)
                nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(1024, affine=True),
                nn.LeakyReLU(0.2, inplace=True),
                    
                # State (512x8x8)
                nn.Conv2d(in_channels=1024, out_channels=2048, kernel_size=4, stride=2, padding=1),
                nn.InstanceNorm2d(2048, affine=True),
                nn.LeakyReLU(0.2, inplace=True))
                # output of main module --> State (1024x4x4)
                    
                
            self.output = nn.Sequential(
                # The output of D is no longer a probability, we do not apply sigmoid at the output of D.
                nn.Conv2d(in_channels=2048, out_channels=1, kernel_size=4, stride=1, padding=0))
            
        else:
            raise TypeError("unsupported image size")

    def forward(self, depth_img, condition_img):
        x = torch.cat((depth_img, condition_img), 1)
        x = self.main_module(x)
        return self.output(x)

    def feature_extraction(self, x):
        # Use discriminator for feature extraction then flatten to vector of 16384
        x = self.main_module(x)
        return x.view(-1, 1024*4*4)


class CWGAN(object):
    def __init__(self, args):
        print("CWGAN init model.")
        self.G = Generator(args.channels)
        self.D = Discriminator(args.channels)
        self.C = args.channels
        self.continue_training = args.continue_training
        self.IM_SIZE = args.rescale

        # Check if cuda is available
        self.check_cuda(args.cuda)

        # WGAN values from paper
        self.learning_rate = 1e-4
        self.b1 = 0.5
        self.b2 = 0.999
        self.batch_size = 64

        # WGAN_gradient penalty uses ADAM
        self.d_optimizer = optim.Adam(self.D.parameters(), lr=self.learning_rate, betas=(self.b1, self.b2))
        self.g_optimizer = optim.Adam(self.G.parameters(), lr=self.learning_rate, betas=(self.b1, self.b2))

        # Set the logger
#        self.logger = Logger('./logs')
#        self.logger.writer.flush()
#        self.number_of_images = 10

        self.generator_iters = args.generator_iters
        self.critic_iter = 5
        self.lambda_term = 10


    def check_cuda(self, cuda_flag=False):
        if cuda_flag:
            self.cuda_index = 0
            self.cuda = True
            self.D.cuda(self.cuda_index)
            self.G.cuda(self.cuda_index)
            print("Cuda enabled flag: {}".format(self.cuda))
        else:
            self.cuda = False
        


    def train(self, train_loader, training_result_images_path = 'training_result_images/', training_result_models_path='training_result_models/'):
        self.t_begin = t.time()
        
        if not os.path.exists(training_result_models_path):
            os.makedirs(training_result_models_path)
        if not os.path.exists(training_result_images_path):
            os.makedirs(training_result_images_path)
        
        begin = 0
        if self.continue_training:
            begin = self.load_latest_model(training_result_models_path)

        training_range = range(begin+1,self.generator_iters+1)
        if begin != 0:
            with open(os.path.join(training_result_images_path,'error_log.txt'), 'a') as f:
                print("Continued at iter "+str(begin), file=f)
        else:
            with open(os.path.join(training_result_images_path,'error_log.txt'), 'w') as f:
                print("", file=f)

        # Now batches are callable self.data.next()
        self.data = self.get_infinite_batches(train_loader)

#        one = torch.FloatTensor([1])
        one = torch.tensor(1, dtype=torch.float)
        mone = one * -1
        if self.cuda:
            one = one.cuda(self.cuda_index)
            mone = mone.cuda(self.cuda_index)

        for g_iter in training_range:

            # Requires grad, Generator requires_grad = False
            for p in self.D.parameters():
                p.requires_grad = True

            d_loss_real = 0
            d_loss_fake = 0
            Wasserstein_D = 0
            # Train Dicriminator forward-loss-backward-update self.critic_iter times while 1 Generator forward-loss-backward-update
            for d_iter in range(self.critic_iter):
                self.D.zero_grad()

                images, images_2d = self.get_batch()

                # Train discriminator
                # WGAN - Training discriminator more iterations than generator
                # Train with real images
                d_loss_real = self.D(images, images_2d)
                d_loss_real = d_loss_real.mean()
                d_loss_real.backward(mone)

                # Train with fake images
                fake_images = self.G(images_2d)
                d_loss_fake = self.D(fake_images, images_2d)
                d_loss_fake = d_loss_fake.mean()
                d_loss_fake.backward(one)

                # Train with gradient penalty
                gradient_penalty = self.calculate_gradient_penalty(images.data, fake_images.data, images_2d)
                gradient_penalty.backward()


                d_loss = d_loss_fake - d_loss_real + gradient_penalty
                Wasserstein_D = d_loss_real - d_loss_fake
                self.d_optimizer.step()

            # Generator update
            for p in self.D.parameters():
                p.requires_grad = False  # to avoid computation

            self.G.zero_grad()
            # train generator
            # compute loss with fake images
            
            images, images_2d = self.get_batch()
            fake_images = self.G(images_2d)
            g_loss = self.D(fake_images, images_2d
                            )
            g_loss = g_loss.mean()
            g_loss.backward(mone)
            g_cost = -g_loss
            self.g_optimizer.step()

            # Saving model and sampling images every 1000th generator iterations
            if (g_iter) % 1000 == 0:
                self.save_i_model(g_iter, training_result_models_path)
                # # Workaround because graphic card memory can't store more than 830 examples in memory for generating image
                # # Therefore doing loop and generating 800 examples and stacking into list of samples to get 8000 generated images
                # # This way Inception score is more correct since there are different generated examples from every class of Inception model
                # sample_list = []
                # for i in range(125):
                #     samples  = self.data.__next__()
                # #     z = Variable(torch.randn(800, 100, 1, 1)).cuda(self.cuda_index)
                # #     samples = self.G(z)
                #     sample_list.append(samples.data.cpu().numpy())
                # #
                # # # Flattening list of list into one list
                # new_sample_list = list(chain.from_iterable(sample_list))
                # print("Calculating Inception Score over 8k generated images")
                # # # Feeding list of numpy arrays
                # inception_score = get_inception_score(new_sample_list, cuda=True, batch_size=32,
                #                                       resize=True, splits=10)

                # Denormalize images and save them in grid 8x8
#                z = Variable(torch.randn(800, 100, 1, 1))
#                if self.cuda:
#                    z = z.cuda(self.cuda_index)

                samples = self.G(images_2d)
                se_box = torch.mul(images - samples,images - samples)
                mse = np.mean(se_box.data.cpu().numpy())
                ae_box = torch.abs(images - samples)
                mae = np.mean(ae_box.data.cpu().numpy())
                with open(os.path.join(training_result_images_path,'error_log.txt'), 'a') as f:
                    print("iter_"+str(g_iter).zfill(6)+' MAE: '+str(mae)+' MSE: '+str(mse), file=f)

                images_2d = images_2d.mul(0.5).add(0.5)
                samples = samples.mul(0.5).add(0.5)
                images = images.mul(0.5).add(0.5)
#                samples = samples.data.cpu()[:64]
                
                grid = utils.make_grid(samples)
                utils.save_image(grid, os.path.join(training_result_images_path,'img_generator_iter_{}_fake.png'.format(str(g_iter).zfill(6))))
                grid = utils.make_grid(images)
                utils.save_image(grid, os.path.join(training_result_images_path,'img_generator_iter_{}_real.png'.format(str(g_iter).zfill(6))))
                grid = utils.make_grid(images_2d)
                utils.save_image(grid, os.path.join(training_result_images_path,'img_generator_iter_{}_2d.png'.format(str(g_iter).zfill(6))))

                # combined img
                img_sample = torch.cat((images_2d.data, samples.data, images.data), -2)
                utils.save_image(img_sample, os.path.join(training_result_images_path,'img_generator_iter_{}.png'.format(str(g_iter).zfill(6))), nrow=8, normalize=True)

                # Testing
                time = t.time() - self.t_begin
                #print("Real Inception score: {}".format(inception_score))
                print("Generator iter: {}".format(g_iter))
                print("Time: {}".format(time))
                print('MAE: '+str(mae))
                print('MSE: '+str(mse))

                # Write to file inception_score, gen_iters, time
                #output = str(g_iter) + " " + str(time) + " " + str(inception_score[0]) + "\n"
                #self.file.write(output)


# =============================================================================
#                 # ============ TensorBoard logging ============#
#                 # (1) Log the scalar values
#                 info = {
#                     'Wasserstein distance': Wasserstein_D.data.item(),
#                     'Loss D': d_loss.data.item(),
#                     'Loss G': g_cost.data.item(),
#                     'Loss D Real': d_loss_real.data.item(),
#                     'Loss D Fake': d_loss_fake.data.item()
# 
#                 }
# 
#                 for tag, value in info.items():
#                     self.logger.scalar_summary(tag, value, g_iter + 1)
# 
#                 # (3) Log the images
#                 info = {
#                     'images_2d': self.real_images(images_2d, self.number_of_images), 
#                     'real_images': self.real_images(images, self.number_of_images),
#                     'generated_images': self.generate_img(images_2d, self.number_of_images)
#                 }
# 
#                 for tag, images in info.items():
#                     self.logger.image_summary(tag, images, g_iter + 1)
# =============================================================================



        self.t_end = t.time()
        print('Time of training-{}'.format((self.t_end - self.t_begin)))
        #self.file.close()

        # Save the trained parameters
        self.save_model()

    def evaluate(self, test_loader, D_model_path, G_model_path, method='MSE', output_dir='evaluation_result/', log_file = 'log.txt'):
        self.load_model(D_model_path, G_model_path)
        
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        with open(os.path.join(output_dir,log_file), 'w') as f:
            print("", file=f)
            
        loss = dict()
        for i, (images, images_2d) in enumerate(test_loader):
            # Check for batch to have full batch_size
            if (images.size()[0] != self.batch_size):
                break
            
            if self.cuda:
                images = Variable(images.cuda(self.cuda_index))
                images_2d = Variable(images_2d.cuda(self.cuda_index))
            else:
                images = Variable(images)
                images_2d = Variable(images_2d)
        
            samples = self.G(images_2d)
            images = images.mul(0.5).add(0.5)
            print('sample min max', np.amin(samples.data.numpy()), np.amax(samples.data.numpy()))
            print('images min max', np.amin(images.data.numpy()), np.amax(images.data.numpy()))
            ae_box = torch.abs(images - samples)
            mae = torch.mean(ae_box)
            min_ae = torch.min(ae_box)
            max_ae = torch.max(ae_box)
            sample_range = np.amax(ae_box.data.numpy())-np.amin(ae_box.data.numpy())
            output_range = np.single(1)
            ae_box = ((ae_box-min_ae)*(output_range)/(sample_range))
            
            l = torch.nn.functional.mse_loss(samples,images).data.numpy()
            with open(os.path.join(output_dir,log_file), 'a') as f:
                print("Completed batch "+str(i)+" which has a MSE of "+str(l)+" and a MAE of "+str(mae.data.numpy()), file=f)
                print("Lowest AE "+str(min_ae.data.numpy())+" Highest AE "+str(max_ae.data.numpy()), file=f)
            loss[i] = l
            
            images_2d = images_2d.mul(0.5).add(0.5)
            samples = samples.mul(0.5).add(0.5)
            images = images.mul(0.5).add(0.5)
            
            grid = utils.make_grid(samples)
            utils.save_image(grid, os.path.join(output_dir,'test_batch_{}_fake.png'.format(str(i).zfill(3))))
            grid = utils.make_grid(images)
            utils.save_image(grid, os.path.join(output_dir,'test_batch_{}_real.png'.format(str(i).zfill(3))))
            grid = utils.make_grid(images_2d)
            utils.save_image(grid, os.path.join(output_dir,'test_batch_{}_2d.png'.format(str(i).zfill(3))))
            grid = utils.make_grid(ae_box)
            utils.save_image(grid, os.path.join(output_dir,'test_batch_{}_SE.png'.format(str(i).zfill(3))))
#            grid = utils.make_grid(samples)
#            print("Grid of 8x8 images saved to 'dgan_model_image.png'.")
#            utils.save_image(grid, 'dgan_model_image.png')
            
            img_sample = torch.cat((images_2d.data, samples.data, images.data), -2)
            utils.save_image(img_sample, os.path.join(output_dir, 'test_batch_{}.png'.format(str(i).zfill(3))), nrow=12, normalize=True)
            img_sample = torch.cat((images_2d.data, samples.data, images.data, ae_box.data), -2)
            utils.save_image(img_sample, os.path.join(output_dir, 'test_batch_{}_combined.png'.format(str(i).zfill(3))), nrow=16, normalize=True)
            
        loss_list = list(loss.values())
        loss_keys = list(loss.keys())
        mse = np.mean(loss_list)
        max_v = np.amax(loss_list)
        max_i = loss_list.index(max_v)
        max_k = loss_keys[max_i]
        min_v = np.amin(loss_list)
        min_i = loss_list.index(min_v)
        min_k = loss_keys[min_i]
        with open(os.path.join(output_dir,log_file), 'a') as f:
            print("Overall MSE: "+str(mse), file=f)
            print("Highest MSE found in batch: "+str(max_k)+" which has a MSE of "+str(max_v), file=f)
            print("Lowest MSE found in batch: "+str(min_k)+" which has a MSE of "+str(min_v), file=f)
            
    def error_progress_evaluate(self, test_loader, training_result_models_path='training_result_models/', output_dir='evaluation_result/', save_png=True, output_min=np.single(-1), output_max=np.single(1)):
        generators = list()
        for root,dirs,files in os.walk(training_result_models_path):
            for file in files:
                if file.endswith("generator.pkl"):
                    i = int(file[5:11])
                    generators.append(i)
        generators.sort()
        
        mse = list()
        mae = list()
        
                    
        for g_iter in generators:
            mse_temp = list()
            mae_temp = list()
            self.load_i_model(g_iter, training_result_models_path)
            for tbatch in iter(test_loader):
                images, images_2d = tbatch[:2]
                
                
                samples = self.G(images_2d)
                
                images = ((images-np.single(-1))*(output_max-output_min)/(np.single(2)))+output_min
                samples = ((samples-np.single(-1))*(output_max-output_min)/(np.single(2)))+output_min
    #            print('sample min max', np.amin(samples.data.numpy()), np.amax(samples.data.numpy()))
    #            print('images min max', np.amin(images.data.numpy()), np.amax(images.data.numpy()))
    #            print('images_2d min max', np.amin(images_2d.data.numpy()), np.amax(images_2d.data.numpy()))
                
                mse_temp.append(torch.nn.functional.mse_loss(samples,images).data.numpy())
                mae_temp.append(torch.nn.functional.l1_loss(samples,images).data.numpy())
                
            print(np.mean(mse_temp))
            mse.append(np.mean(mse_temp))
            mae.append(np.mean(mae_temp))
        if save_png:
            plt.close()
            plt.figure()
            # Plotting the data
            plt.plot(generators, mse, label='MSE')
            plt.plot(generators, mae, label='MAE')
            plt.ylabel('Error')
            plt.xlabel('Generator Iterations')
            #plt.ylim(top=0.8)
            # Adding a legend
            plt.legend()
            # Result
            plt.savefig(os.path.join(output_dir,'error_progress.png'), dpi=144)
        return(generators, mse, mae)
            
            
                        
                        
        
    def find_match(self, test_loader, D_model_path, G_model_path, method='AE'):
        self.load_model(D_model_path, G_model_path)
        z = Variable(torch.randn(self.batch_size, 100, 1, 1))
        
        with torch.no_grad():
            samples = self.G(z)
            closestmatches = list()
            if method == 'AE':
                for s in samples:
                    closestmatches.append({"sample":s[0].numpy(),
                                           "match": np.full((64,64), -100),
                                           "distance":np.sum(np.abs(np.full((64,64), -100) - s[0].numpy()))
                                               })
            elif method == 'SE':
                    closestmatches.append({"sample":s[0].numpy(),
                                           "match": np.full((64,64), -100),
                                           "distance":np.sum(np.square(np.full((64,64), -100) - s[0].numpy()))
                                               })
        match_dir = "matches/"
        if not os.path.exists(match_dir):
            os.makedirs(match_dir)

        for i in range(len(closestmatches)):
            item = closestmatches[i]
#            item["sample"]
#            item["match"]
#            item["distance"]
            
            for batch in test_loader:
                for p in batch[0]:
                    test_sample = p[0].numpy()
                    if method == 'AE':
                        test_sample_distance =  np.sum(np.abs(test_sample - item["sample"]))
                    elif method == 'SE':
                        test_sample_distance =  np.sum(np.abs(test_sample - item["sample"]))
                        
                
                    if test_sample_distance < item["distance"]:
                        item["match"] = test_sample
                        item["distance"] = test_sample_distance

            print("saving closest match "+str(i).zfill(3))
            s = Image.fromarray(item["sample"])
            m = Image.fromarray(item["match"])
            imageio.imwrite(os.path.join(match_dir, "sample_"+str(i).zfill(3)+"_s.png"), s)
            imageio.imwrite(os.path.join(match_dir, "sample_"+str(i).zfill(3)+"_m.png"), m)


    def make_fakes(self, train_loader, test_loader, D_model_path, G_model_path, output_dir="make_fakes_output"):
        self.load_model(D_model_path, G_model_path)
        mm_per_pixel = 165 * 0.67 / self.IM_SIZE
        
        for loadertype,loader in zip(["test","train"],[test_loader,train_loader]):
            for i, testbatch in enumerate(loader):
                images_3d, images_2d, image_class, image_person, image_index = testbatch
                fake_3d = self.G(images_2d)
                
                for j, im_2d in enumerate(images_2d):
                    fpath = os.path.join(output_dir, loadertype, image_class[j])
                    if not os.path.exists(fpath):
                        os.makedirs(fpath)
                    
                    fname_2d = os.path.join(fpath, image_person[j]+'d'+image_index[j]+"_2d")
                    fname_real_3d = os.path.join(fpath, image_person[j]+'d'+image_index[j]+"_real3d")
                    fname_fake_3d = os.path.join(fpath, image_person[j]+'d'+image_index[j]+"_fake3d")

                    np_2d = self.to_np(im_2d[0])
                    np_3d_real = self.to_np(images_3d[j][0])
                    np_3d_fake = self.to_np(fake_3d[j][0])

                    old_3d_min = np.single(-37.27238464355469)
                    old_3d_max = np.single(142.7432861328125)
                    
                    internal_range = np.single(2)
                    output_range = old_3d_max-old_3d_min
                    np_3d_real = ((np_3d_real-np.single(-1))*(output_range)/(internal_range))+old_3d_min
                    np_3d_fake = ((np_3d_fake-np.single(-1))*(output_range)/(internal_range))+old_3d_min
                    np_2d = ((np_2d-np.single(-1))*(255)/(internal_range))

                    
                    sfi_2d = SFI.from_numpy(np_2d)
                    sfi_real_3d = SFI.from_numpy(np_3d_real)
                    sfi_fake_3d = SFI.from_numpy(np_3d_fake)
                    
                    sfi_2d.write_to_file(fname_2d+'.sfi')
                    sfi_real_3d.write_to_file(fname_real_3d+'.sfi')
                    sfi_fake_3d.write_to_file(fname_fake_3d+'.sfi')

                    sfi_2d.save_as_png(fname_2d+'.png', dynamic_range=False, input_range = (0,255))
                    sfi_real_3d.save_as_png(fname_real_3d+'.png', dynamic_range=False, input_range = (old_3d_min,old_3d_max))
                    sfi_fake_3d.save_as_png(fname_fake_3d+'.png', dynamic_range=False, input_range = (old_3d_min,old_3d_max))
                              
                    abs_real_3d = ABS.from_sfi(sfi_real_3d, mm_per_pixel=mm_per_pixel, invert=True)
                    abs_real_3d.fix_too_small()
                    abs_real_3d.write_to_file(fname_real_3d+'.abs')
                    abs_fake_3d = ABS.from_sfi(sfi_fake_3d, mm_per_pixel=mm_per_pixel, invert=True)
                    abs_fake_3d.fix_too_small()
                    abs_fake_3d.write_to_file(fname_fake_3d+'.abs')
                    
                    wrl_real_3d = WRL.from_sfi(sfi_real_3d, sfi_2d, mm_per_pixel=mm_per_pixel, invert=True)
                    wrl_real_3d.fix_too_small()
                    wrl_real_3d.write_to_file(fname_real_3d+'.wrl')
                    wrl_fake_3d = WRL.from_sfi(sfi_fake_3d, sfi_2d, mm_per_pixel=mm_per_pixel, invert=True)
                    wrl_fake_3d.fix_too_small()
                    wrl_fake_3d.write_to_file(fname_fake_3d+'.wrl')


    def calculate_gradient_penalty(self, real_images, fake_images, images_2d):
        eta = torch.FloatTensor(self.batch_size,1,1,1).uniform_(0,1)
        eta = eta.expand(self.batch_size, real_images.size(1), real_images.size(2), real_images.size(3))
        if self.cuda:
            eta = eta.cuda(self.cuda_index)
        else:
            eta = eta

        interpolated = eta * real_images + ((1 - eta) * fake_images)

        if self.cuda:
            interpolated = interpolated.cuda(self.cuda_index)
        else:
            interpolated = interpolated

        # define it to calculate gradient
        interpolated = Variable(interpolated, requires_grad=True)

        # calculate probability of interpolated examples
        prob_interpolated = self.D(interpolated, images_2d)

        # calculate gradients of probabilities with respect to examples
        gradients = autograd.grad(outputs=prob_interpolated, inputs=interpolated,
                               grad_outputs=torch.ones(
                                   prob_interpolated.size()).cuda(self.cuda_index) if self.cuda else torch.ones(
                                   prob_interpolated.size()),
                               create_graph=True, retain_graph=True)[0]

        grad_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * self.lambda_term
        return grad_penalty

    def real_images(self, images, number_of_images):
        h = self.IM_SIZE
        w = self.IM_SIZE
        if (self.C == 3):
            return self.to_np(images.view(-1, self.C, h, w)[:self.number_of_images])
        else:
            return self.to_np(images.view(-1, h, w)[:self.number_of_images])

    def generate_img(self, z, number_of_images):
        h = self.IM_SIZE
        w = self.IM_SIZE
        samples = self.G(z).data.cpu().numpy()[:number_of_images]
        generated_images = []
        for sample in samples:
            if self.C == 3:
                generated_images.append(sample.reshape(self.C, h, w))
            else:
                generated_images.append(sample.reshape(h, w))
        return generated_images

    def to_np(self, x):
        return x.data.cpu().numpy()

    def save_model(self, g_path='./generator.pkl', d_path='./discriminator.pkl'):
        torch.save(self.G.state_dict(), g_path)
        torch.save(self.D.state_dict(), d_path)
        print('Models save to ' + g_path + ' & ' + d_path)
    
    def save_i_model(self, i, training_result_models_path = 'training_result_models/'):
        g_path = os.path.join(training_result_models_path, 'iter_{}_generator.pkl'.format(str(i).zfill(6)))
        d_path = os.path.join(training_result_models_path, 'iter_{}_discriminator.pkl'.format(str(i).zfill(6)))
        self.save_model(g_path, d_path)
        

    def load_model(self, D_model_filename, G_model_filename):
        D_model_path = os.path.join(os.getcwd(), D_model_filename)
        G_model_path = os.path.join(os.getcwd(), G_model_filename)
        if self.cuda:
            self.D.load_state_dict(torch.load(D_model_path))
            self.G.load_state_dict(torch.load(G_model_path))
        else:
            device = torch.device('cpu')
            self.D.load_state_dict(torch.load(D_model_path, map_location=device))
            self.G.load_state_dict(torch.load(G_model_path, map_location=device))
        print('Generator model loaded from {}.'.format(G_model_path))
        print('Discriminator model loaded from {}-'.format(D_model_path))
        
    def load_i_model(self, i, training_result_models_path = 'training_result_models/'):
        g_path = os.path.join(training_result_models_path, 'iter_{}_generator.pkl'.format(str(i).zfill(6)))
        d_path = os.path.join(training_result_models_path, 'iter_{}_discriminator.pkl'.format(str(i).zfill(6)))
        self.load_model(d_path, g_path)
        
    
    def load_latest_model(self, training_result_models_path = 'training_result_models/'):
        highest_iter = 0
        if os.path.exists(training_result_models_path):
            for root,dirs,files in os.walk(training_result_models_path):
                    for file in files:
                        if file.endswith("generator.pkl"):
                            i = int(file[5:11])
                            if i > highest_iter:
                                highest_iter = i
            if highest_iter > 0:
                self.load_i_model(highest_iter)
        
        return highest_iter

    def get_infinite_batches(self, data_loader):
        while True:
            for i, d in enumerate(data_loader):
                (images, images_2d) = d[:2]
                yield images, images_2d
                
    def get_batch(self):
        images, images_2d = self.data.__next__()
        # Check for batch to have full batch_size
        if (images.size()[0] != self.batch_size):
            return self.get_batch()

        if self.cuda:
            images = Variable(images.cuda(self.cuda_index))
            images_2d = Variable(images_2d.cuda(self.cuda_index))
        else:
            images = Variable(images)
            images_2d = Variable(images_2d)
            
        return images, images_2d

    
    def generate_latent_walk(self, number):
        if not os.path.exists('interpolated_images/'):
            os.makedirs('interpolated_images/')

        number_int = 10
        # interpolate between twe noise(z1, z2).
        z_intp = torch.FloatTensor(1, 100, 1, 1)
        z1 = torch.randn(1, 100, 1, 1)
        z2 = torch.randn(1, 100, 1, 1)
        if self.cuda:
            z_intp = z_intp.cuda()
            z1 = z1.cuda()
            z2 = z2.cuda()

        z_intp = Variable(z_intp)
        images = []
        alpha = 1.0 / float(number_int + 1)
        print(alpha)
        for i in range(1, number_int + 1):
            z_intp.data = z1*alpha + z2*(1.0 - alpha)
            alpha += alpha
            fake_im = self.G(z_intp)
            fake_im = fake_im.mul(0.5).add(0.5) #denormalize
            images.append(fake_im.view(self.C,32,32).data.cpu())

        grid = utils.make_grid(images, nrow=number_int )
        utils.save_image(grid, 'interpolated_images/interpolated_{}.png'.format(str(number).zfill(3)))
        print("Saved interpolated images.")
