#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 12:29:17 2020

@author: reesink93
"""
import os
import random

root_dir = r'E:\Datasets\frgc_reg_source'
output_dir = r'E:\Datasets\frgc_reg_halved'
test_dir = os.path.join(root_dir,'test')
train_dir = os.path.join(root_dir,'train')

print(train_dir)
persons = set()
for root,dirs,files in os.walk(train_dir):
    for file in files:
        persons.add(os.path.join(root[len(train_dir)+1:],file.split('d')[0]))
        
        
        

train_persons = list(persons)
print('Found '+str(len(train_persons))+' persons')
half_size = int(len(train_persons)*0.5)
print('Picking '+ str(half_size)+ ' random persons for half dataset')
half_persons = set()
while len(half_persons) < half_size:
    c = random.choice(train_persons)
    half_persons.add(c)
    train_persons.remove(c)
half_persons = list(half_persons)



for root,dirs,files in os.walk(train_dir):
    for d in dirs:
        for new in [output_dir]:
            new_dir = os.path.join(new, os.path.join(root,d)[len(train_dir)+1:])
            if not os.path.exists(new_dir):
                os.makedirs(new_dir)
        

for root,dirs,files in os.walk(train_dir):
    for file in files:
        if os.path.join(root[len(train_dir)+1:],file.split('d')[0]) in half_persons:
            source = os.path.join(root,file)
            dest = os.path.join(output_dir,
                                root[len(train_dir)+1:],
                                file)
            os.rename(source, dest)