#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 12:29:17 2020

@author: reesink93
"""
import os
import random

root_dir = os.path.join('.','unsorted')
persons = set()
for root,dirs,files in os.walk(root_dir):
    for file in files:
        persons.add(os.path.join(root[len(root_dir)+1:],file.split('d')[0]))
        
persons = list(persons)
test_persons = set()
while len(test_persons) < 99:
    c = random.choice(persons)
    test_persons.add(c)
    persons.remove(c)
test_persons = list(test_persons)


for root,dirs,files in os.walk(root_dir):
    for d in dirs:
        for new in [os.path.join('.','test'), os.path.join('.','train')]:
            new_dir = os.path.join(new, os.path.join(root,d)[len(root_dir)+1:])
            if not os.path.exists(new_dir):
                os.makedirs(new_dir)
        

for root,dirs,files in os.walk(root_dir):
    for file in files:
        if os.path.join(root[len(root_dir)+1:],file.split('d')[0]) in test_persons:
            source = os.path.join(root,file)
            dest = os.path.join('.','test',
                                root[len(root_dir)+1:],
                                file)
            os.rename(source, dest)
        else:
            source = os.path.join(root,file)
            dest = os.path.join('.','train',
                                root[len(root_dir)+1:],
                                file)
            os.rename(source, dest)