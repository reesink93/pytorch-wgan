#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 12:29:17 2020

@author: reesink93
"""
import os
import random

root_dir = '/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Datasets/frgc_reg_splitted'
unsorted_dir = os.path.join(root_dir,'unsorted')
test_dir = os.path.join(root_dir,'test')
train_dir = os.path.join(root_dir,'train')

persons = set()
for root,dirs,files in os.walk(unsorted_dir):
    for file in files:
        persons.add(os.path.join(root[len(unsorted_dir)+1:],file.split('d')[0]))
        
persons = list(persons)
print('Found '+str(len(persons))+' persons')
test_size = int(len(persons)*0.1)
print('Picking '+ str(test_size)+ ' random persons for testing')
test_persons = set()
while len(test_persons) < test_size:
    c = random.choice(persons)
    test_persons.add(c)
    persons.remove(c)
test_persons = list(test_persons)


for root,dirs,files in os.walk(unsorted_dir):
    for d in dirs:
        for new in [test_dir, train_dir]:
            new_dir = os.path.join(new, os.path.join(root,d)[len(unsorted_dir)+1:])
            if not os.path.exists(new_dir):
                os.makedirs(new_dir)
        

for root,dirs,files in os.walk(unsorted_dir):
    for file in files:
        if os.path.join(root[len(unsorted_dir)+1:],file.split('d')[0]) in test_persons:
            source = os.path.join(root,file)
            dest = os.path.join(test_dir,
                                root[len(unsorted_dir)+1:],
                                file)
            os.rename(source, dest)
        else:
            source = os.path.join(root,file)
            dest = os.path.join(train_dir,
                                root[len(unsorted_dir)+1:],
                                file)
            os.rename(source, dest)