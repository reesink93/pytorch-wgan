#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 11:28:32 2020

@author: reesink93
"""

import sys,os
from PIL import Image
import math

def main(root_dir,samples_per_row=8):
    pictures = dict()
    for root,dirs,files in os.walk(root_dir ):
        for file in files:
            if file.endswith("s.png"):
                pictures[file[:-6]] = [Image.open(os.path.join(root,file)),Image.open(os.path.join(root,file[:-5]+"m.png"))]

    
    rows = math.ceil(len(pictures)/samples_per_row)

#    for key,[s,m] in pictures.items():
#        print(key)
#        print(s)
#        print(m)
    widths, heights = zip(*(s.size for key,[s,m] in pictures.items()))

    total_width = samples_per_row * 2 * max(widths) + (samples_per_row-1) * 2
    total_height = rows * max(heights) + (rows-1) * 2
    
    new_im = Image.new('L', (total_width, total_height), color = 255)
    
    x_offset = 0
    y_offset = 0
    for key,[s,m] in pictures.items():
        new_im.paste(s, (x_offset,y_offset))
        new_im.paste(m, (x_offset+s.size[0],y_offset))
        x_offset += s.size[0] + m.size[0] + 2
        if x_offset >= total_width:
            x_offset = 0
            y_offset += s.size[1] + 2
    
    new_im.save('combined.png')

if __name__ == '__main__':
    main('.')