#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:46:55 2020

@author: reesink93
"""
import struct
import numpy as np
import os
import matplotlib.pyplot as plt

def test():
#    dataset_dir = "/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Datasets/frgc_reg_splitted/"
#    train_dir = os.path.join(dataset_dir, 'train')
#    season_dir = os.path.join(train_dir, 'Fall2003range_reg')
#    test_file_2d = os.path.join(season_dir, '02463d546.abs.sfi')
#    test_file_3d = os.path.join(season_dir, '02463d547.ppm.sfi')
    test_file_2d = 'in.ppm.sfi'
    test_file_3d = 'in.abs.sfi'    
    test_outfile_2d = 'out.ppm.sfi'
    test_outfile_3d = 'out.abs.sfi'
    im_2d = read_sfi_from_file(test_file_2d)
    im_3d = read_sfi_from_file(test_file_3d)
    im_out_2d = read_sfi_from_file(test_outfile_2d)
    im_out_3d = read_sfi_from_file(test_outfile_3d)
    
    print(np.amin(im_2d),np.amax(im_2d))
    print(np.amin(im_3d),np.amax(im_3d))
    plt.figure()
    plt.imshow(im_2d)
    plt.show()
    plt.figure()
    plt.imshow(im_3d)
    plt.show()
    
    plt.figure()
    plt.imshow(im_out_2d)
    plt.show()
    plt.figure()
    plt.imshow(im_out_3d)
    plt.show()
    
#    write_sfi_to_file('out.ppm.sfi', im_2d)
#    write_sfi_to_file('out.abs.sfi', im_3d)

def write_sfi(f, im):
    ftype = 'CSU_SFI'
    
    if len(im.shape) == 2:
        channels = 1
    else: 
        raise TypeError("Can only convert 1 channel images")
        channels = im.shape[2]
    
    height = im.shape[0]
    width = im.shape[1]
    
    firstline = [ftype, width, height, channels]
    firstline = [bytes(str(x), 'utf-8') for x in firstline]
    firstline = b' '.join(firstline)+b'\n'
        
    image=b''
    for j in range(height):
        for i in range(width):
            image += struct.pack(">f", im[j][i])

    sfi = firstline + image
    f.write(sfi)

def write_sfi_to_file(fname, im):
    """
    Args:
        fname (String): the location of the file to be decoded
    """
    
    # little open file wrapper
    try:
        with open(fname,'wb') as f:
            return write_sfi(f, im)
    except FileNotFoundError:
        print("Can't open", fname)
    except TypeError as e:
        print(e)
        

def read_sfi(f):
    """
    Args:
        f (BufferedReader): sfi file in binary mode
    """
    
    # read until a newline character is found
    firstline = b''
    while True:
        b = f.read(1)
        if b == b'\n':
            break
        firstline += b
    # split on spaces and extract file parameters
    firstline = firstline.split(b' ') 
    ftype = firstline[0]
    width = int(firstline[1])
    height = int(firstline[2])
    channels = int(firstline[3])
    
    # verify compatibility
    if ftype != b'CSU_SFI':
        raise TypeError("Incompatible File Type")
    if channels != 1:
        raise TypeError("Can only convert 1 channel images")
    
    # read file into numpy array, decode float values as big endian float
    im = np.full((height,width),-1,dtype='double')
    for j in range(height):
        for i in range(width):
            im[j][i]=struct.unpack(">f",f.read(4))[0]
    return im

def sfi_to_pgm(im):
    """
    Args:
        im (Numpy array): sfi file as numpy array
    """
    
    # transform the data to be between 0 and 255
    # the numpy array gets returned as uint8 as this is the actual range
    im_min = np.amin(im)
    im_max = np.amax(im)
    im_range = im_max-im_min
    return ((im-im_min)*255/(im_range)).astype('uint8')

def read_sfi_from_file(fname):
    """
    Args:
        fname (String): the location of the file to be decoded
    """
    
    # little open file wrapper
    try:
        with open(fname,'rb') as f:
            return read_sfi(f)
    except FileNotFoundError:
        print("Can't open", fname)
    except TypeError as e:
        print(e)
        
        
if __name__ == '__main__':
    if any('SPYDER' in name for name in os.environ):
        test()    
