#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 17:40:43 2020

@author: reesink93
"""

import numpy as np
import matplotlib as plt
import gzip
import re
import struct
from PIL import Image

class ABS():
    def __init__(self, flag = None, x = None, y = None, z = None, data = None, fname = None, compressed=False):
        if fname != None:
            self.read_from_file(fname, compressed)
            
        elif data != None:
            self.data = data
            self.rows, self.columns = next(iter(data.values()))
        else:
            self.__rows, self.__columns = flag.shape
            
            self.data = {
                    'flag':flag,
                    'X':x,
                    'Y':y,
                    'Z':z}
            
            
    @staticmethod
    def from_sfi(sfi, mm_per_pixel = 0.67, invert=False):
        isinstance(sfi, (SFI,))
        im = sfi.get_data()
        
        flag = np.full(im.shape, 1, dtype='int')
        x    = np.full(im.shape, -999999, dtype='float')
        y    = np.full(im.shape, -999999, dtype='float')
        z    = im
        
        rows, columns = im.shape
        left = (columns * mm_per_pixel /2)
        bottom = (rows * mm_per_pixel / 2)
        for r in range(rows):
            for c in range(columns):
                x[r][c] = c*mm_per_pixel - left
                y[r][c] = r*mm_per_pixel - bottom
        
        if invert:
            y = y*np.float(-1)
            z = z*np.float(-1)
            
        return ABS(flag= flag, x = x, y = y, z = z)
    
    
    @staticmethod
    def from_file(fname, compressed = False):
        return ABS(fname=fname, compressed = compressed)
    
    
    def fix_too_small(self):
        flag,x,y,z = self.data.values()
        
        flag_top = np.full((1,flag.shape[0]), 0, dtype='int')
        flag_bottom = np.full((1,flag.shape[0]), 0, dtype='int')
        flag_top[0][0]=1
        flag_bottom[0][0]=1
        flag = np.vstack((flag_top,flag,flag_bottom))
        
        x_top = np.full((1,x.shape[1]), 0, dtype='float')
        y_top = np.full((1,y.shape[1]), 70, dtype='float')
        z_top = np.full((1,z.shape[1]), 0, dtype='float')
        
        x_bottom = np.full((1,x.shape[1]), 0, dtype='float')
        y_bottom = np.full((1,y.shape[1]), -70, dtype='float')
        z_bottom = np.full((1,z.shape[1]), 0, dtype='float')
        
        x = np.vstack((x_top,x,x_bottom))
        y = np.vstack((y_top,y,y_bottom))
        z = np.vstack((z_top,z,z_bottom))
        
        
        self.__rows, self.__columns = flag.shape
        
        self.data = {
                    'flag':flag,
                    'X':x,
                    'Y':y,
                    'Z':z}
        
    
    def write_to_file(self, fname, compressed=False):
        # little open file wrapper
        if compressed:
            try:
                with gzip.open(fname,'wt') as f:
                    return self.__write(f)
            except FileNotFoundError:
                print("Can't open", fname)
            except TypeError as e:
                print(e)
        else:
            try:
                with open(fname,'w') as f:
                    return self.__write(f)
            except FileNotFoundError:
                print("Can't open", fname)
            except TypeError as e:
                print(e)
        
    
    def __write(self, f):
        """
        Args:
            f (BufferedReader): abs file
            im (Dictionary): data representation of ABS
        """
        
        f.write(str(self.__rows) + ' rows\n')
        f.write(str(self.__columns) + ' columns\n')
        f.write('pixels ('+' '.join(self.data.keys())+'):')

        for key, data in self.data.items():
            f.write('\n')
            if 'float' in str(data.dtype):
                for row in range(self.__rows):
                    for column in range(self.__columns):
                        value = data[row][column]
                        f.write( np.format_float_positional(value, unique = False, precision=6) + ' ')
            else:
                for row in range(self.__rows):
                    for column in range(self.__columns):
                        value = data[row][column]
                        f.write( str(value) + ' ')
                
    def plot(self):
        im = dict()
        im['flag'] = self.data.get('flag')
        im['X'] = np.where(self.data['flag'] == 0 , -200, im['X'])
        im['Y'] = np.where(self.data['flag'] == 0 , -200, im['Y'])
        im['Z'] = np.where(self.data['flag'] == 0 , -2500, im['Z'])
        
        for label, img in im.items():
            plt.figure()
            plt.title(label)
            plt.imshow(img)
            plt.show()

            
    
    def __read(self, f):
        """
        Args:
            f (BufferedReader): sfi file in binary mode
        """
        
        # first lines contain the rows and column numbers
        self.__rows = int(f.readline().split(' ')[0])
        self.__columns = int(f.readline().split(' ')[0])
        
        #third line has the channel labels within ()
        pattern = "\((.*?)\)"
        labels = re.search(pattern, f.readline()).group(1)
    
        self.data = dict()
    
        for label in labels.split(' '):
            if label == 'flag':
                a = np.full((self.__rows,self.__columns), -1, dtype= 'int')
            else:
                a =  np.full((self.__rows,self.__columns), -1, dtype= 'double')
            
            values = f.readline().split(' ')
            for row in range(self.__rows):
                for column in range(self.__columns):
                    a[row][column] = values[self.__columns*row+column]
            
            self.data[label] = a
        
        return self.data
    
    
    
    def read_from_file(self, fname, compressed=False):
        if compressed:
            try:
                with gzip.open(fname,'rt') as f:
                    return self.__read(f)
            except FileNotFoundError:
                print("Can't open", fname)
            except TypeError as e:
                print(e)
        else:
            try:
                with open(fname,'r') as f:
                    return self.__read(f)
            except FileNotFoundError:
                print("Can't open", fname)
            except TypeError as e:
                print(e)

class SFI():
    def __init__(self, data = None, fname = None):
        if fname is not None:
            self.read_from_file(fname)
            
        elif data is not None:
            self.data = data
            
        else:
            self.data = np.full((2,2), -1)

    @staticmethod
    def from_numpy(sfi):
        return SFI(data= sfi)
    
    
    @staticmethod
    def from_file(fname):
        return SFI(fname=fname)
    
    
    @staticmethod
    def from_abs(im):
        raise TypeError('Conversion currently unsupported')
        return im

    
    def get_data(self):
        return self.data


    def __write(self, f):
        """
        Args:
            f (BufferedReader): sfi file in binary mode
        """
        ftype = 'CSU_SFI'
        
        if len(self.data.shape) == 2:
            channels = 1
        else: 
            raise TypeError("Can only convert 1 channel images")
            channels = self.data.shape[2]
        
        height = self.data.shape[0]
        width = self.data.shape[1]
        
        firstline = [ftype, width, height, channels]
        firstline = [bytes(str(x), 'utf-8') for x in firstline]
        firstline = b' '.join(firstline)+b'\n'
            
        image=b''
        for j in range(height):
            for i in range(width):
                image += struct.pack(">f", self.data[j][i])
    
        sfi = firstline + image
        f.write(sfi)
    
    def write_to_file(self, fname):
        """
        Args:
            fname (String): the location of the file to be encoded
        """
        
        # little open file wrapper
        try:
            with open(fname,'wb') as f:
                return self.__write(f)
        except FileNotFoundError:
            print("Can't open", fname)
        except TypeError as e:
            print(e)
            
    
    def __read(self, f):
        """
        Args:
            f (BufferedReader): sfi file in binary mode
        """
        
        # read until a newline character is found
        firstline = b''
        while True:
            b = f.read(1)
            if b == b'\n':
                break
            firstline += b
        # split on spaces and extract file parameters
        firstline = firstline.split(b' ') 
        ftype = firstline[0]
        width = int(firstline[1])
        height = int(firstline[2])
        channels = int(firstline[3])
        
        # verify compatibility
        if ftype != b'CSU_SFI':
            raise TypeError("Incompatible File Type")
        if channels != 1:
            raise TypeError("Can only convert 1 channel images")
        
        # read file into numpy array, decode float values as big endian float
        self.data = np.full((height,width),-1,dtype='double')
        for j in range(height):
            for i in range(width):
                self.data[j][i]=struct.unpack(">f",f.read(4))[0]
        return self.data
    
    
    def read_from_file(self, fname):
        """
        Args:
            fname (String): the location of the file to be decoded
        """
        
        # little open file wrapper
        try:
            with open(fname,'rb') as f:
                return self.__read(f)
        except FileNotFoundError:
            print("Can't open", fname)
        except TypeError as e:
            print(e)
            
            
    def to_pgm(self, input_range = (-1,1) ):
        """
        Args:
            im (Numpy array): sfi file as numpy array
        """
        
        # transform the data to be between 0 and 255
        # the numpy array gets returned as uint8 as this is the actual range
        im_min, im_max = input_range
        im_range = im_max-im_min
        return ((self.data-im_min)*255/(im_range)).astype('uint8')
    
    def save_as_png(self, fname, dynamic_range= True, input_range = (-1,1)):
        if dynamic_range:
            im_min = np.amin(self.data)
            im_max = np.amax(self.data)
            im = self.to_pgm((im_min,im_max))
        else:
            im = self.to_pgm(input_range)

        im = Image.fromarray(im)
        im.save(fname)
        

class WRL():
    def __init__(self, data = None, colors = None, fname = None):
        if fname != None:
            self.read_from_file(fname)
            
        elif data != None:
            isinstance(data,(list,))
            self.data = data
            self.colors = None
            if colors != None:
                isinstance(colors, (list,))
                if len(colors)== len(data):
                    self.colors = colors
            if self.colors == None:
                self.colors = [(0.5,0.5,0.5) for i in range(len(data))]
                
        else:
            self.data = list()
        
    
    @staticmethod
    def from_numpy(im_3d, im_2d=None, mm_per_pixel = 0.67, invert=False):
        newdata = list()
        newcolors = None
        
        rows, columns = im_3d.shape
        left = (columns * mm_per_pixel /2)
        bottom = (rows * mm_per_pixel / 2)
        for r in range(rows):
            for c in range(columns):
                x = c*mm_per_pixel - left
                y= r*mm_per_pixel - bottom
                z = im_3d[r][c]
                
                if invert:
                    y = y*np.float(-1)
                    z = z*np.float(-1)
                    
                newdata.append((x,y,z))
        
        if im_2d is not None:
            newcolors = list()
            
            for r in range(rows):
                for c in range(columns):
                    color = im_2d[r][c] / 255
                    x = color
                    y = color
                    z = color
                    newcolors.append((x,y,z))
        
        return WRL(data=newdata, colors=newcolors)
    
    @staticmethod
    def from_sfi(sfi_3d, sfi_2d = None, mm_per_pixel = 0.67, invert=False):
        isinstance(sfi_3d, (SFI,))
        
        im_3d = sfi_3d.get_data()
        im_2d = None
        if sfi_2d is not None:
            im_2d = sfi_2d.get_data()
            
        return WRL.from_numpy(im_3d=im_3d, im_2d = im_2d, mm_per_pixel = mm_per_pixel, invert=invert)
    
    
    @staticmethod
    def from_file(fname):
        raise TypeError("Currently unsuported")
        return 
    
    
    @staticmethod
    def from_abs(im):
        isinstance(im, ABS)
        newdata = [(x,y,z) for x,y,z,flag in zip(im.data['X'].flatten(),im.data['Y'].flatten(), im.data['Z'].flatten(), im.data['flag'].flatten()) if flag == 1]

        return WRL(data=newdata)

    
    def get_data(self):
        return self.data
    
    
    def get_colors(self):
        return self.colors


    def fix_too_small(self):
        self.data.append((0.,-70.,0.))
        self.data.append((0.,70.,0.))
        self.colors.append((0.5,0.5,0.5))
        self.colors.append((0.5,0.5,0.5))
        

    def __write(self, f):
        """
        Args:
            f (BufferedReader): sfi file in binary mode
        """

        f.write("#VRML V2.0 utf8"+'\n')
        f.write("Transform {"+ '\n')
        f.write("\t"+ "rotation 1 0 0 3.14159265" + '\n')
        f.write('\t\t'+ "children ["+'\n')
        f.write("\t\t\t"+ "Shape {" + '\n' )
        f.write('\t\t'+ "appearance Appearance {"+ '\n')
        f.write('\t\t'+'}'+'\n')
        f.write('\t\t'+"geometry PointSet {"+ '\n')
        f.write('\t\t\t'+"coord Coordinate {"+ '\n')
        
        f.write('\t\t\t\t'+ "point ")
        self.__write_list(f,self.data, tabs = 5)
        
        
        f.write('\t\t\t'+ '}'+ '\n')

        #Write Color info, required to show the wrl in Luuks VRML shower
        f.write('\t\t\t'+ "color Color {" + '\n')
        f.write('\t\t\t\t'+"color ")
        
        self.__write_list(f, self.colors, tabs = 5)
        
        f.write("\t\t\t"+'}'+'\n')
        

        #End the file
        f.write('\t\t'+'}'+ "\n")
        f.write('\t\t\t'+'}'+ "\n")
        f.write('\t\t'+']'+ "\n")
        f.write(''+'}'+ "\n")
        

    def __write_list(self,f,l, tabs = 5):
        f.write("[" + '\n')
        
        lines = list()
        for xyz in l:
            text = [str(x) for x in xyz]
            lines.append('\t'*tabs + ' '.join(text))

        f.write(',\n'.join(lines)+'\n')
        f.write('\t'*(tabs-1)+']'+'\n')


    def write_to_file(self, fname):
        """
        Args:
            fname (String): the location of the file to be encoded
        """
        
        # little open file wrapper
        try:
            with open(fname,'w') as f:
                return self.__write(f)
        except FileNotFoundError:
            print("Can't open", fname)
        except TypeError as e:
            print(e)
            
    
    def __read(self, f):
        """
        Args:
            f (BufferedReader): sfi file in binary mode
        """
        
        return
    
    
    def read_from_file(self, fname):
        """
        Args:
            fname (String): the location of the file to be decoded
        """
        
        # little open file wrapper
        try:
            with open(fname,'r') as f:
                return self.__read(f)
        except FileNotFoundError:
            print("Can't open", fname)
        except TypeError as e:
            print(e)
