import torchvision.datasets as dset
import torchvision.transforms as transforms
import torch.utils.data as data_utils
import numpy as np
from utils.fashion_mnist import MNIST, FashionMNIST
import utils.sfi_loader as sfi_loader
import utils.sfireg_loader as sfireg_loader

def get_data_loader(args):

    if args.dataset == 'mnist':
        trans = transforms.Compose([
            transforms.Scale(args.rescale),
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,)),
        ])
        train_dataset = MNIST(root=args.dataroot, train=True, download=args.download, transform=trans)
        test_dataset = MNIST(root=args.dataroot, train=False, download=args.download, transform=trans)

    elif args.dataset == 'fashion-mnist':
        trans = transforms.Compose([
            transforms.Scale(args.rescale),
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,)),
        ])
        train_dataset = FashionMNIST(root=args.dataroot, train=True, download=args.download, transform=trans)
        test_dataset = FashionMNIST(root=args.dataroot, train=False, download=args.download, transform=trans)

    elif args.dataset == 'cifar':
        trans = transforms.Compose([
            transforms.Scale(args.rescale),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])

        train_dataset = dset.CIFAR10(root=args.dataroot, train=True, download=args.download, transform=trans)
        test_dataset = dset.CIFAR10(root=args.dataroot, train=False, download=args.download, transform=trans)

    elif args.dataset == 'stl10':
        trans = transforms.Compose([
            transforms.Resize(args.rescale),
            transforms.ToTensor(),
        ])
        train_dataset = dset.STL10(root=args.dataroot, train=True, download=args.download, transform=trans)
        test_dataset = dset.STL10(root=args.dataroot, train=False, download=args.download, transform=trans)

    elif args.dataset == 'sfi':
        trans = transforms.Compose([
                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
                    sfi_loader.Rescale3D(args.rescale),
                    sfi_loader.CenterCrop3D(args.rescale), 
                    sfi_loader.AddDimension(),
                    sfi_loader.ToTensor()
                    ])

        train_dataset = sfi_loader.Face3dDataset(root=args.dataroot, train=True, download=args.download, transform=trans)
        test_dataset = sfi_loader.Face3dDataset(root=args.dataroot, train=False, download=args.download, transform=trans)
    
    elif args.dataset == 'sfi2d':
        trans = transforms.Compose([
                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
                    sfi_loader.Rescale3D(args.rescale),
                    sfi_loader.CenterCrop3D(args.rescale), 
                    sfi_loader.Rescale2D(args.rescale),
                    sfi_loader.CenterCrop2D(args.rescale), 
                    sfi_loader.Grayscale2D(), 
                    sfi_loader.ToRange2D(-1.,1.),
                    sfi_loader.AddDimension(),
                    sfi_loader.ToTensor()
                    ])
    
    
        train_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)

    elif args.dataset == 'sfireg':
        trans = transforms.Compose([
                    sfireg_loader.ToRange(),
                    sfireg_loader.Rescale(args.rescale),
                    sfireg_loader.CenterCrop(args.rescale),
#                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
#                    sfireg_loader.Rescale3D(args.rescale),
#                    sfireg_loader.CenterCrop3D(args.rescale), 
#                    sfireg_loader.Rescale2D(args.rescale),
#                    sfireg_loader.CenterCrop2D(args.rescale), 
#                    sfireg_loader.Grayscale2D(), 
#                    sfireg_loader.ToRange2D(-1.,1.),
                    sfireg_loader.AddDimension(),
                    sfireg_loader.ToTensor()
                    ])
    
    
        train_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)
 
    elif args.dataset == 'sfinoisy2d':
        trans = transforms.Compose([
                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
                    sfi_loader.Rescale3D(args.rescale),
                    sfi_loader.CenterCrop3D(args.rescale), 
                    sfi_loader.Rescale2D(args.rescale),
                    sfi_loader.CenterCrop2D(args.rescale), 
                    sfi_loader.Grayscale2D(), 
                    sfi_loader.ToRange2D(-1.,1.),
                    sfi_loader.Noisy2d(),
                    sfi_loader.AddDimension(),
                    sfi_loader.ToTensor()
                    ])

        train_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)
 
    elif args.dataset == 'sfinoisyreg':
        trans = transforms.Compose([
                    sfireg_loader.ToRange(),
                    sfireg_loader.Rescale(args.rescale),
                    sfireg_loader.CenterCrop(args.rescale),
                    sfi_loader.Noisy2d(),
#                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
#                    sfireg_loader.Rescale3D(args.rescale),
#                    sfireg_loader.CenterCrop3D(args.rescale), 
#                    sfireg_loader.Rescale2D(args.rescale),
#                    sfireg_loader.CenterCrop2D(args.rescale), 
#                    sfireg_loader.Grayscale2D(), 
#                    sfireg_loader.ToRange2D(-1.,1.),
                    sfireg_loader.AddDimension(),
                    sfireg_loader.ToTensor()
                    ])
    
    
        train_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)
    
    elif args.dataset == 'sfitwiceasnoisy2d':
        trans = transforms.Compose([
                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
                    sfi_loader.Rescale3D(args.rescale),
                    sfi_loader.CenterCrop3D(args.rescale), 
                    sfi_loader.Rescale2D(args.rescale),
                    sfi_loader.CenterCrop2D(args.rescale), 
                    sfi_loader.Grayscale2D(), 
                    sfi_loader.ToRange2D(-1.,1.),
                    sfi_loader.Noisy2d(g_mean=0, g_var=0.002),
                    sfi_loader.AddDimension(),
                    sfi_loader.ToTensor()
                    ])

        train_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)
  
    elif args.dataset == 'sfitwiceasnoisyreg':
        trans = transforms.Compose([
                    sfireg_loader.ToRange(),
                    sfireg_loader.Rescale(args.rescale),
                    sfireg_loader.CenterCrop(args.rescale),
                    sfi_loader.Noisy2d(g_mean=0, g_var=0.002),
#                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
#                    sfireg_loader.Rescale3D(args.rescale),
#                    sfireg_loader.CenterCrop3D(args.rescale), 
#                    sfireg_loader.Rescale2D(args.rescale),
#                    sfireg_loader.CenterCrop2D(args.rescale), 
#                    sfireg_loader.Grayscale2D(), 
#                    sfireg_loader.ToRange2D(-1.,1.),
                    sfireg_loader.AddDimension(),
                    sfireg_loader.ToTensor()
                    ])
    
    
        train_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)

    elif args.dataset == 'sfiextranoisy2d':
        trans = transforms.Compose([
                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
                    sfi_loader.Rescale3D(args.rescale),
                    sfi_loader.CenterCrop3D(args.rescale), 
                    sfi_loader.Rescale2D(args.rescale),
                    sfi_loader.CenterCrop2D(args.rescale), 
                    sfi_loader.Grayscale2D(), 
                    sfi_loader.ToRange2D(-1.,1.),
                    sfi_loader.Noisy2d(g_mean=0, g_var=0.005),
                    sfi_loader.AddDimension(),
                    sfi_loader.ToTensor()
                    ])

        train_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfi_loader.Face3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)
  
    elif args.dataset == 'sfiextranoisyreg':
        trans = transforms.Compose([
                    sfireg_loader.ToRange(),
                    sfireg_loader.Rescale(args.rescale),
                    sfireg_loader.CenterCrop(args.rescale),
                    sfi_loader.Noisy2d(g_mean=0, g_var=0.005),
#                    sfi_loader.SfiToRange('FRGC', output_min=np.single(-1), output_max=np.single(1)),
#                    sfireg_loader.Rescale3D(args.rescale),
#                    sfireg_loader.CenterCrop3D(args.rescale), 
#                    sfireg_loader.Rescale2D(args.rescale),
#                    sfireg_loader.CenterCrop2D(args.rescale), 
#                    sfireg_loader.Grayscale2D(), 
#                    sfireg_loader.ToRange2D(-1.,1.),
                    sfireg_loader.AddDimension(),
                    sfireg_loader.ToTensor()
                    ])
    
    
        train_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=True, test = False, download=args.download, transform=trans, load_2d = True, load_3d = True)
        test_dataset = sfireg_loader.RegisteredFace3dDataset(root=args.dataroot, root_2d = args.dataroot_2d, train=False, test = True, download=args.download, transform=trans, load_2d = True, load_3d = True)
   

    elif args.dataset == 'celeba-gray':
        trans = transforms.Compose([
                    transforms.Resize(args.rescale),
                    transforms.CenterCrop(args.rescale),
                    transforms.Grayscale(),
                    transforms.ToTensor(),
                    transforms.Normalize(mean=(0.5, ), std=(0.5, ))
                ])
        train_dataset = dset.CelebA(root=args.dataroot, split="train", download=args.download, transform=trans)
        test_dataset = dset.CelebA(root=args.dataroot, split="test", download=args.download, transform=trans)
        
    elif args.dataset == 'celeba':
        trans = transforms.Compose([
                    transforms.Resize(args.rescale),
                    transforms.CenterCrop(args.rescale),
                    transforms.ToTensor(),
                    transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
                ])
    
        train_dataset = dset.CelebA(root=args.dataroot, split="train", download=args.download, transform=trans)
        test_dataset = dset.CelebA(root=args.dataroot, split="test", download=args.download, transform=trans)

    # Check if everything is ok with loading datasets
    assert train_dataset
    assert test_dataset

    train_dataloader = data_utils.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True)
    test_dataloader  = data_utils.DataLoader(test_dataset,  batch_size=args.batch_size, shuffle=True)

    return train_dataloader, test_dataloader
