import argparse
import os

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def parse_args():
    parser = argparse.ArgumentParser(description="Pytorch implementation of GAN models.")

    parser.add_argument('--model', type=str, default='DCGAN', choices=['GAN', 'DCGAN', 'WGAN-CP', 'WGAN-GP', 'CWGAN', 'CWGAN-NOISY'])
    parser.add_argument('--is_train', type=str2bool, default=True)
    parser.add_argument('--find_match', type=str2bool, default=False)
    parser.add_argument('--make_fakes', type=str2bool, default=False)
    parser.add_argument('--dataroot', required=True, help='path to dataset')
    parser.add_argument('--dataroot_2d', required=False,default='', help='path to 2D dataset')
    parser.add_argument('--dataset', type=str, default='mnist', choices=['mnist', 'fashion-mnist', 'cifar', 'stl10','sfi', 'sfireg','sfi2d','sfinoisy2d', 'sfitwiceasnoisy2d', 'sfiextranoisy2d','sfinoisyreg', 'sfitwiceasnoisyreg', 'sfiextranoisyreg','celeba','celeba-gray'],
                            help='The name of dataset')
    parser.add_argument('--download', type=str2bool, default=False)
    parser.add_argument('--epochs', type=int, default=50, help='The number of epochs to run')
    parser.add_argument('--rescale', type=int, default=32, help='The size of input and output images')
    parser.add_argument('--batch_size', type=int, default=64, help='The size of batch')
    parser.add_argument('--cuda',  type=str2bool, default=False, help='Availability of cuda')

    parser.add_argument('--continue_training', type=str2bool, default=True, help="If true then continue from last stored model")
    parser.add_argument('--load_D', type=str, default='discriminator.pkl', help='Path for loading Discriminator network')
    parser.add_argument('--load_G', type=str, default='generator.pkl', help='Path for loading Generator network')
    parser.add_argument('--generator_iters', type=int, default=10000, help='The number of iterations for generator in WGAN model.')
    parser.add_argument('--make_fakes_output_dir', type=str, default='make_fakes_output')
    return check_args(parser.parse_args())

# Checking arguments
def check_args(args):
    # --epoch
    try:
        assert args.epochs >= 1
    except:
        print('Number of epochs must be larger than or equal to one')

    # --batch_size
    try:
        assert args.batch_size >= 1
    except:
        print('Batch size must be larger than or equal to one')

    if args.dataset == 'cifar' or args.dataset == 'stl10' or args.dataset == 'celeba':
        args.channels = 3
    else:
        args.channels = 1

    return args
