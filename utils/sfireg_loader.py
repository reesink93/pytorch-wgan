# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 13:16:41 2019

@author: reesi
"""

import sys
import struct
import numpy as np
import torchvision.transforms as transforms
from torchvision.utils import save_image
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable
import torch
import os
import matplotlib.pyplot as plt
import cv2
from PIL import Image
from .sfi_reader import read_sfi_from_file

#%% Dataset Class
class RegisteredFace3dDataset(Dataset):
    """3D Faces dataset."""

    def __init__(self, root, transform=None, root_2d="", train=False, test=False, download=False, load_2d=False, load_3d=True):
        """
        Args:
            root (string): Directory with all the 3d images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
            train (boolean): Does nothing, there for compliance
            download (boolean): Does nothing, there for compliance
        """
        self.root_dir = root
        if root_2d:
            self.root_dir_2d = root_2d
        else:
            self.root_dir_2d = self.root_dir
        self.transform = transform
        self.data = list()
        self.train = train
        self.test = test
        self.download = download
        self.load_2d = load_2d
        self.load_3d = load_3d

        # scan the 3d folder for files and save sfi file locations together with the ppm file locations
        # class is based on the directory
        # path 2d is based on a file number increment by 1 and a ppm extension
        # the file numbers have a leading zero if smaller than 10
        if self.train:
            self.scan_dir(os.path.join(self.root_dir, 'train'),os.path.join(self.root_dir_2d, 'train'))
        if self.test:
            self.scan_dir(os.path.join(self.root_dir, 'test'),os.path.join(self.root_dir_2d, 'test'))
            
    def scan_dir(self, root_dir, rood_dir_2d):
        for root,dirs,files in os.walk(root_dir):
                for file in files:
                    person = file[:-len('.abs.sfi')].split('d')[0]
                    
                    image_index = int(file[:-len('.abs.sfi')].split('d')[1])
                    image_index_str = str(image_index).zfill(2)
                    fname_2d = person +'d'+str(image_index+1).zfill(2)+'.ppm.sfi'
                    
                    if file.endswith(".abs.sfi"): 
                        self.data.append({
                                "class":root[len(root_dir)+1:],
                                "person":person,
                                "im_i":image_index,
                                "im_i_str":image_index_str,
                                "fname":file,
                                "fname_2d":fname_2d,
                                "path": os.path.join(root, file),
                                "path_2d":os.path.join(rood_dir_2d,
                                                       root[len(root_dir)+1:],
                                                       fname_2d)
                                })
    
    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        """
        Args:
            idx (int): Index of item
        """
        if torch.is_tensor(idx):
            idx = idx.tolist()
        d = self.data[idx]
        
        # read the files into a numpy uint8 array
        # if !load then make a 2x2 array filled with 0
        if self.load_3d:
            im = read_sfi_from_file(d.get("path"))
        else:
            im = np.full((2,2,1),0,dtype='double')
        if self.load_2d:
            im_2d = read_sfi_from_file(d.get("path_2d"))
        else:
            im_2d = np.full((2, 2, 1),0,dtype='double')


        sample = (im, im_2d)

        # if transformation, excecute
        if self.transform:
            (im, im_2d) = self.transform(sample)

        return (im, im_2d, d.get("class"), d.get("person"), d.get("im_i_str"))

#%% Transformers
class Grayscale2D(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, num_output_channels=1):
        self.num_output_channels = num_output_channels

    def __call__(self, sample):
        im, im_2d = sample
        im_2d = Image.fromarray(im_2d)
        if self.num_output_channels == 1:
            im_2d = im_2d.convert('L')
        elif self.num_output_channels == 3:
            im_2d = im_2d.convert('L')
            np_img = np.array(im_2d, dtype=np.uint8)
            np_img = np.dstack([np_img, np_img, np_img])
            im_2d = Image.fromarray(np_img, 'RGB')
        im_2d = np.asarray(im_2d)


        return (im, im_2d)


class ToRange(object):
    
    
    def __init__(self, new_min=np.single(-1), new_max=np.single(1), 
                         old_2d_min=0, old_2d_max=255, 
                         old_3d_min = np.single(-37.27238464355469), old_3d_max=np.single(142.7432861328125), 
                         dtype='single', inplace=False):
        
        self.transform2d = ToRange2D(new_min= new_min, new_max=new_max, old_min=old_2d_min, old_max=old_2d_max, dtype=dtype, inplace=inplace)
        self.transform3d = ToRange3D(dataset='custom', output_min=new_min, output_max=new_max, sample_min=old_3d_min, sample_max=old_3d_max)
        
    def __call__(self, sample):
        return self.transform2d(self.transform3d(sample))
        
    
        
    

class ToRange2D(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, new_min, new_max, old_min=0, old_max=255, dtype='single', inplace=False):
        self.new_max = new_max
        self.new_min = new_min
        self.old_min = old_min
        self.old_max = old_max
        self.dtype = dtype
        self.inplace = inplace

    def __call__(self, sample):
        im, im_2d = sample
        
        
        old_range = self.old_max-self.old_min
        new_range = self.new_max-self.new_min
        

        if not self.inplace:
            im_2d = np.copy(im_2d)
        
        im_2d = im_2d.astype(self.dtype)
        
        im_2d = (im_2d - self.old_min)/(old_range)*new_range+self.new_min
        

        return (im, im_2d)    
    
class ToRange3D(object):
    
    def __init__(self, dataset, output_min=np.single(-1), output_max=np.single(1), sample_min=0, sample_max=100):
        if dataset == 'custom':
            self.sample_min = sample_min
            self.sample_max = sample_max
        elif dataset == 'FRGC':
            self.sample_min = np.single(-24.739604949951172)
            self.sample_max = np.single( 93.25985717773438)
        else:
            raise ValueError("Dataset must be string name of either a known dataset or 'custom'")
        self.output_min = output_min
        self.output_max = output_max
            
    def __call__(self, sample):
        im, im_2d = sample
        sample_range = self.sample_max-self.sample_min
        output_range = self.output_max-self.output_min
        im = ((im-self.sample_min)*(output_range)/(sample_range))+self.output_min
    
        return (im, im_2d)
    

class Noisy2d(object):
    """Adds Noise to the 2D image

    Args:
        noise String: Desired type of noise
            'gauss'     Gaussian-distributed additive noise.
            'poisson'   Poisson-distributed noise generated from the data.
            's&p'       Replaces random pixels with 0 or 1.
            'speckle'   Multiplicative noise using out = image + n*image,where
                        n is uniform noise with specified mean & variance.
    """

    def __init__(self, noise="gauss", g_mean=0, g_var=0.001):
        if noise == "gauss":
            self.noise = self.gauss_noise
        elif noise == "poisson":
            self.noise = self.poisson_noise
        elif noise == "s&p":
            self.noise = self.sp_noise
        elif noise == "speckle":
            self.noise = self.speckle_noise
        else:
            raise ValueError("unknown noise type")
            
        self.g_mean = g_mean
        self.g_var = g_var

    def __call__(self, sample):
        im, im_2d = sample
        im_2d = self.noise(im_2d)
        
        return (im, im_2d)
    
    def gauss_noise(self, img):
        sigma = self.g_var**0.5
        gauss = np.random.normal(self.g_mean,sigma,img.shape)
        gauss = gauss.reshape(*img.shape)
        noisy = img + gauss
        
        return noisy
    
    def poisson_noise(self, img):
        raise TypeError("currently unsupported for our image type")
        vals = len(np.unique(img))
        vals = 2 ** np.ceil(np.log2(vals)).astype('single')
        noisy = np.random.poisson(img * vals) / vals
        return noisy

    
    def sp_noise(self, img, s_vs_p = 0.5, amount = 0.004):
        out = np.copy(img)
        # Salt mode
        num_salt = np.ceil(amount * img.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                for i in img.shape]
        out[coords] = 1

        # Pepper mode
        num_pepper = np.ceil(amount* img.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                for i in img.shape]
        out[coords] = 0
        
        return out
    
    def speckle_noise(self, img):
        gauss = np.random.randn(*img.shape)
        gauss = gauss.reshape(*img.shape)        
        noisy = img + img * gauss
        return noisy

class Rescale(object):
    def __init__(self, output_size):
        self.transform2d = Rescale2D(output_size)
        self.transform3d = Rescale3D(output_size)
        
    def __call__(self, sample):
        return self.transform2d(self.transform3d(sample))


class Rescale2D(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        im, im_2d = sample

        h, w = im_2d.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

#        img = transform.resize(im_2d, (new_h, new_w))
        im_2d = cv2.resize(im_2d, dsize=(new_w, new_h), interpolation=cv2.INTER_LINEAR)


        return (im, im_2d)
    
class Rescale3D(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        im, im_2d = sample

        h, w = im.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

#        img = transform.resize(im, (new_h, new_w))
        im = cv2.resize(im, dsize=(new_w, new_h), interpolation=cv2.INTER_LINEAR)

        return (im, im_2d)


class RandomCrop2D(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        im, im_2d = sample

        h, w = im_2d.shape[:2]
        new_h, new_w = self.output_size
        
        top,left = 0,0
        if h - new_h !=0:
            top = np.random.randint(0, h - new_h)
        if w - new_w !=0:
            left = np.random.randint(0, w - new_w)

        im_2d = im_2d[top: top + new_h,
                      left: left + new_w]


        return (im, im_2d)
    
class RandomCrop3D(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        im, im_2d = sample

        h, w = im.shape[:2]
        new_h, new_w = self.output_size
        
        top,left = 0,0
        if h - new_h !=0:
            top = np.random.randint(0, h - new_h)
        if w - new_w !=0:
            left = np.random.randint(0, w - new_w)

        im = im[top: top + new_h,
                      left: left + new_w]


        return (im, im_2d)

class CenterCrop(object):
    def __init__(self, output_size):
        self.transform2d = CenterCrop2D(output_size)
        self.transform3d = CenterCrop3D(output_size)
        
    def __call__(self, sample):
        return self.transform2d(self.transform3d(sample))


class CenterCrop2D(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        im, im_2d = sample

        h, w = im_2d.shape[:2]
        new_h, new_w = self.output_size
        
        top = int((h - new_h) /2)
        left = int((w - new_w) /2)

        im_2d = im_2d[top: top + new_h,
                      left: left + new_w]


        return (im, im_2d)
    
class CenterCrop3D(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        im, im_2d = sample

        h, w = im.shape[:2]
        new_h, new_w = self.output_size
        
        top = int((h - new_h) /2)
        left = int((w - new_w) /2)
        
        im = im[top: top + new_h,
                      left: left + new_w]

        return (im, im_2d)
    
class SfiToPGM(object):
    def __call__(self, sample):
        
        im, im_2d = sample
        im = sfi_to_pgm(im).astype('uint8')
        return (im, im_2d)
        

    
class AddDimension(object):
    def __call__(self, sample):
        im, im_2d = sample
        if len(im.shape) < 3:
            im = np.expand_dims(im, axis=2).astype('single')
        if len(im_2d.shape) < 3:
            im_2d = np.expand_dims(im_2d, axis=2).astype('single')
        return (im, im_2d)
    
class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        im, im_2d = sample

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        im = im.transpose((2, 0, 1))
        im = torch.from_numpy(im)
        
        im_2d = im_2d.transpose((2, 0, 1))
        im_2d = torch.from_numpy(im_2d)
            
        return (im, im_2d)
        

#%% Show batch helper
def show_sfi_batch(sample_batched):
    """Show image with landmarks for a batch of samples."""
    images_batch, images_2d_batch = \
            sample_batched
    for i in range(len(images_batch)):
        image = images_batch[i]
        image_2d = images_2d_batch[i]
        if len(image.shape)>2 and image.shape[0]==1:
            image = np.squeeze(image, axis=0)
        if len(image_2d.shape)>2 and image_2d.shape[0]==1:
            image_2d = np.squeeze(image_2d, axis=0)
        else:
            image_2d = image_2d.permute(1, 2, 0)
            
        ax = plt.subplot(2, len(images_batch), i + 1)
        plt.tight_layout()
        ax.set_title('Sample #{}'.format(i))
        ax.axis('off')
        plt.imshow(image)
        ax = plt.subplot(2, len(images_batch), i + 1 + len(images_batch))
        ax.set_title('Sample #{}'.format(i))
        ax.axis('off')
        plt.imshow(image_2d)   
        plt.show(block=True)     
        
        
def sample_images(batches_done, test_loader, generator, cuda = False):
    """Saves a generated sample from the validation set"""
    Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
    (images_3d, images_2d) = next(iter(test_loader))
    
    with torch.no_grad():
        
        real_A = Variable(images_2d.type(Tensor))
        real_B = Variable(images_3d.type(Tensor))
        fake_B = generator(real_A)
        img_sample = torch.cat((real_A.data, fake_B.data, real_B.data), -2)
        save_image(img_sample, "images/%s/%s.png" % ('SFI', batches_done), nrow=5, normalize=True)
