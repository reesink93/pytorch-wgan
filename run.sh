python main.py --model WGAN-GP \
               --is_train True \
               --download True \
               --dataroot /home/reesink93/datasets/mnist \
               --dataset mnist \
               --generator_iters 40000 \
               --cuda True \
               --batch_size 64

