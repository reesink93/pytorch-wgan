#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 10:33:13 2020

@author: reesink93
"""

import utils.sfi_loader as sfi
import os
import random
import numpy as np

root_dir = '/home/reesink93/datasets/3dfaces_splitted/test'

persons = set()
for root,dirs,files in os.walk(root_dir):
    for file in files:
        persons.add(os.path.join(root[len(root_dir)+1:],file.split('d')[0]))

persons = list(persons)

depth_images = dict()
for p in persons:
    depth_images[p] = list()

normalizer = sfi.SfiToRange('FRGC')

for root,dirs,files in os.walk(root_dir):
    for file in files:
        p = os.path.join(root[len(root_dir)+1:],file.split('d')[0])
        img = sfi.read_sfi_from_file(os.path.join(root,file))
        depth_images[p].append(normalizer((img,_))[0])

mses = list()
while len(mses) < 10:
    dpn1 = random.choice(persons)
    dpn2 = random.choice(persons)
    if dpn1 == dpn2:
        continue
    dp1 = depth_images[dpn1]
    dp2 = depth_images[dpn2]
    di1 = random.choice(dp1)
    di12 = random.choice(dp1)
    di2 = random.choice(dp2)

    mse_different = ((di1 - di2)**2).mean(axis=None)
    
    mse_same = ((di1 - di12)**2).mean(axis=None)
    if  mse_same == 0.:
        continue
    mses.append((mse_same, mse_different))
