#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 13:41:38 2020

@author: reesink93
"""

from utils.config import parse_args
from utils.data_loader import get_data_loader
from utils.sfi_loader import show_sfi_batch
import sys
import os
from PIL import Image

from models.gan import GAN
from models.dcgan import DCGAN_MODEL
from models.wgan_clipping import WGAN_CP
from models.wgan_gradient_penalty import WGAN_GP
from models.cwgan_gradient_penalty import CWGAN
from models.cwgan_inc_noise import CWGAN_NOISY
from torchvision import utils
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch import autograd
import numpy as np
import torchvision.transforms as transforms
import io
import matplotlib.pyplot as plt
import scipy.misc
import imageio
import torchvision.datasets as dset
import utils.sfi_loader as sfi_loader
import torch.utils.data as data_utils


import main as original_main

if any('SPYDER' in name for name in os.environ):
    sys.argv.extend(['--model', 'CWGAN', 
                     '--is_train', 'False', 
                     '--continue_training', 'False',
                     '--find_match', 'False', 
                     '--make_fakes', 'True',
                     '--download', 'True', 
                     '--dataroot', "/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Datasets/frgc_reg_splitted/", 
#                     '--dataroot', 'E:\data', 
#                     '--dataset', 'sfiextranoisy2d', 
                     '--dataset', 'sfireg', 
                     '--generator_iters', '40000', 
                     '--epochs', '500',
                     '--rescale', '64',
                     '--cuda', 'False', 
                     '--batch_size', '64'])
    
def main(args):
#    model = None
#    if args.model == 'GAN':
#        model = GAN(args)
#    elif args.model == 'DCGAN':
#        model = DCGAN_MODEL(args)
#    elif args.model == 'WGAN-CP':
#        model = WGAN_CP(args)
#    elif args.model == 'WGAN-GP':
#        model = WGAN_GP(args)
#    elif args.model == 'CWGAN':
#        model = CWGAN(args)
#    elif args.model == 'CWGAN-NOISY':
#        model = CWGAN_NOISY(args)
#    else:
#        print("Model type non-existing. Try again.")
#        exit(-1)

    # Load datasets to train and test loaders
    train_loader, test_loader = get_data_loader(args)
    
    
    
    
    
    print(len(train_loader))
    print(len(test_loader))
#    find_minmax([train_loader, test_loader])
    
    for batch in train_loader:
        images_3d, images_2d, image_class, image_person, image_index = batch
        
        z = Variable(torch.randn(images_2d.size()))
        output = torch.cat((images_3d, z), -2)
        
        utils.save_image(output, 'batchincnoise.png', nrow=8, normalize=True)

        break
    




            
    
def find_minmax(loaders):
     min_2d =  999999
     max_2d = -999999
     min_3d =  999999
     max_3d = -999999
     for loader in loaders:
         for batch in loader:
             im_2d_batch = batch[1]
             im_3d_batch = batch[0]
 #            print(im_2d_batch.shape)
             for sample in im_2d_batch:
                 ma = np.amax(sample.data.numpy())
                 mi = np.amin(sample.data.numpy())
                 
                 if ma > max_2d:
                     max_2d = ma
                 if mi < min_2d:
                     min_2d = mi
                 
                 
             for sample in im_3d_batch:
                 ma = np.amax(sample.data.numpy())
                 mi = np.amin(sample.data.numpy())
 
                 if ma > max_3d:
                     max_3d = ma
                 if mi < min_3d:
                     min_3d = mi
         
     print(min_2d,max_2d)
     print(min_3d, max_3d)
     return min_2d,max_2d,min_3d,max_3d
    #feature_extraction = FeatureExtractionTest(train_loader, test_loader, args.cuda, args.batch_size)



#    # Start model training
#    if args.is_train:
#        model.train(train_loader)
#
#    elif args.find_match:
#        model.find_match(train_loader, args.load_D, args.load_G)
#    # start evaluating on test data
#    else:
##        model.evaluate(test_loader, args.load_D, args.load_G)
#        model.error_progress_evaluate(test_loader)
#        # for i in range(50):
#        #    model.generate_latent_walk(i)
# =============================================================================


    
if __name__ == '__main__':
    args = parse_args()
    main(args)
#    original_main.main(args)