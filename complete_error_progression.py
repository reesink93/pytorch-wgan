#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 16:07:09 2020

@author: reesink93
"""
from utils.config import parse_args
from utils.data_loader import get_data_loader

from models.cwgan_gradient_penalty import CWGAN
from models.cwgan_inc_noise import CWGAN_NOISY
import numpy as np
import json
import sys,os
import matplotlib.pyplot as plt

if any('SPYDER' in name for name in os.environ):
    sys.argv.extend(['--model', 'CWGAN', 
                     '--is_train', 'False', 
                     '--continue_training', 'True',
                     '--find_match', 'False', 
                     '--download', 'True', 
                     '--dataroot', '/home/reesink93/datasets/3dfaces_splitted', 
                     '--dataroot_2d', '/home/reesink93/datasets/FRGC_v2.0/nd1',
#                     '--dataroot', 'E:\data', 
                     '--dataset', 'sfi2d', 
#                     '--dataroot', "/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Datasets/frgc_reg_splitted/", 
#                     '--dataset', 'sfireg', 
                     '--generator_iters', '40000', 
                     '--epochs', '500',
                     '--rescale', '64',
                     '--cuda', 'False', 
                     '--batch_size', '64'])
    
def main(args, scenarios):
    models = {
            'CWGAN':CWGAN(args),
            'CWGAN-NOISY':CWGAN_NOISY(args)
            }


    # Load datasets to train and test loaders
    train_loader, test_loader = get_data_loader(args)

    mse_fig = plt.figure(1)
    plt.ylabel('MSE in mm')
    plt.xlabel('Generator Iterations')
    plt.yscale('log')
#    plt.ylim(0.001,0.035)
    mae_fig = plt.figure(2)
    plt.ylabel('MAE in mm')
    plt.xlabel('Generator Iterations')
    plt.yscale('log')
#    plt.ylim(0.001,0.035)

    for name, scenario in scenarios.items():
        json_path = 'complete_error_unreg_'+name+'.json'
        if not os.path.exists(json_path):
            print("scenario "+name+" does not exist")
            model = models.get(scenario.get('model'))
            path = os.path.join(scenario.get('path'), 'training_result_models/')
            unreg_min = np.single(-24.739604949951172)
            unreg_max = np.single( 93.25985717773438)
            reg_min=np.single(-37.27238464355469)
            reg_max=np.single(142.7432861328125)
            
            generators, mse, mae = model.error_progress_evaluate(test_loader, training_result_models_path=path, save_png=False, output_min=unreg_min, output_max=unreg_max)
            
            mse = [float(x) for x in mse]
            mae= [float(x) for x in mae]
        
            with open(json_path, 'w') as f:
                json.dump([generators, mse, mae], f)
            
        else:
            print("scenario "+name+" exists")
            with open(json_path, 'r') as f:
                [generators, mse, mae] = json.load(f)
            
        
        plt.figure(1)
        plt.plot(generators, mse, label=name)
        
        plt.figure(2)
        plt.plot(generators, mae, label=name)
            
        # for i in range(50):
        #    model.generate_latent_walk(i)

    plt.figure(1)
    plt.legend()
    plt.savefig('complete_mse_error_unreg.png', dpi=144)
    plt.figure(2)
    plt.legend()
    plt.savefig('complete_mae_error_unreg.png', dpi=144)

if __name__ == '__main__':
    args = parse_args()
    scenarios = {
        'CWGAN-NoNoise':{
                'model':'CWGAN',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-2/normal'
                },
        'CWGAN-0.001Var':{
                'model':'CWGAN',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-2/noisy'
                },
        'CWGAN-0.002Var':{
                'model':'CWGAN',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-2/twiceasnoisy'
                },
        'CWGAN-0.005Var':{
                'model':'CWGAN',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-2/extranoisy'
                },
        'CWGAN_NOISY-NoNoise':{
                'model':'CWGAN-NOISY',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-noisy-3/normal'
                },
        'CWGAN_NOISY-0.001Var':{
                'model':'CWGAN-NOISY',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-noisy-3/noisy'
                },
        'CWGAN_NOISY-0.002Var':{
                'model':'CWGAN-NOISY',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-noisy-3/twiceasnoisy'
                },
        'CWGAN_NOISY-0.005Var':{
                'model':'CWGAN-NOISY',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfi-cwgan-noisy-3/extranoisy'
                },
        
        }
    reg_scenarios = {
#        'CWGAN-NoNoise':{
#                'model':'CWGAN',
#                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgan-normal'
#                },
#        'CWGAN-0.001Var':{
#                'model':'CWGAN',
#                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgan-noisy'
#                },
        'CWGAN-0.002Var':{
                'model':'CWGAN',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgan-twiceasnoisy'
                },
#        'CWGAN-0.005Var':{
#                'model':'CWGAN',
#                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgan-extranoisy'
#                },
#        'CWGAN_NOISY-NoNoise':{
#                'model':'CWGAN-NOISY',
#                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgannoisy-normal'
#                },
        'CWGAN_NOISY-0.001Var':{
                'model':'CWGAN-NOISY',
                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgannoisy-noisy'
                },
#        'CWGAN_NOISY-0.002Var':{
#                'model':'CWGAN-NOISY',
#                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgannoisy-twiceasnoisy'
#                },
#        'CWGAN_NOISY-0.005Var':{
#                'model':'CWGAN-NOISY',
#                'path':'/home/reesink93/OneDrive/Vakken/2018-2B Research Topics/Korenvliet/sfireg/cwgannoisy-extranoisy'
#                },
            }
    main(args, scenarios)