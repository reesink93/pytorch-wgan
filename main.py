from utils.config import parse_args
from utils.data_loader import get_data_loader

from models.gan import GAN
from models.dcgan import DCGAN_MODEL
from models.wgan_clipping import WGAN_CP
from models.wgan_gradient_penalty import WGAN_GP
from models.cwgan_gradient_penalty import CWGAN
from models.cwgan_inc_noise import CWGAN_NOISY


def main(args):
    model = None
    if args.model == 'GAN':
        model = GAN(args)
    elif args.model == 'DCGAN':
        model = DCGAN_MODEL(args)
    elif args.model == 'WGAN-CP':
        model = WGAN_CP(args)
    elif args.model == 'WGAN-GP':
        model = WGAN_GP(args)
    elif args.model == 'CWGAN':
        model = CWGAN(args)
    elif args.model == 'CWGAN-NOISY':
        model = CWGAN_NOISY(args)
    else:
        print("Model type non-existing. Try again.")
        exit(-1)

    # Load datasets to train and test loaders
    train_loader, test_loader = get_data_loader(args)
    #feature_extraction = FeatureExtractionTest(train_loader, test_loader, args.cuda, args.batch_size)

    # Start model training
    if args.is_train:
        model.train(train_loader)

    elif args.find_match:
        model.find_match(train_loader, args.load_D, args.load_G)
    # start evaluating on test data
    elif args.make_fakes:
        model.make_fakes(train_loader=train_loader,
                         test_loader=test_loader,
                         D_model_path=args.load_D,
                         G_model_path=args.load_G,
                         output_dir=args.make_fakes_output_dir)
    
    else:
        model.evaluate(test_loader, args.load_D, args.load_G)
        model.error_progress_evaluate(test_loader)
        # for i in range(50):
        #    model.generate_latent_walk(i)


if __name__ == '__main__':
    args = parse_args()
    main(args)